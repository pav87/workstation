#!/bin/bash

set -e

cd ~

echo "Install basic prerequisites"
sudo apt-add-repository -y ppa:ansible/ansible
sudo apt-get update -y
sudo apt-get install -y curl git software-properties-common ansible

git clone https://gitlab.com/pav87/workstation.git && cd workstation

echo "Setup workstation"
ansible-galaxy collection install community.general
ansible-playbook ansible/setup.yaml --extra-vars "ansible_sudo_pass=$1" --ask-vault-pass
