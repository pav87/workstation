import os
import subprocess
from typing import List  # noqa: F401

from libqtile import bar, extension, hook, layout, qtile, widget
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.lazy import lazy

mod = "mod4"
terminal = "gnome-terminal"

BLACK = "#29414f"
RED = "#ec5f67"
GREEN = "#99c794"
YELLOW = "#fac863"
BLUE = "#6699cc"
MAGENTA = "#c594c5"
CYAN = "#5fb3b3"
WHITE = "#ffffff"

colors = [
    ["#1b1c26", "#14151C", "#1b1c26"],  # color 0
    ["#485062", "#485062", "#485062"],  # color 1
    ["#65bdd8", "#65bdd8", "#65bdd8"],  # color 2
    ["#bc7cf7", "#a269cf", "#bc7cf7"],  # color 3
    ["#aed1dc", "#98B7C0", "#aed1dc"],  # color 4
    ["#ffffff", "#ffffff", "#ffffff"],  # color 5
    ["#bb94cc", "#AB87BB", "#bb94cc"],  # color 6
    ["#9859B3", "#8455A8", "#9859B3"],  # color 7
    ["#744B94", "#694486", "#744B94"],  # color 8
    ["#0ee9af", "#0ee9af", "#0ee9af"],
]  # color 9


# ICONS LIST
# , , ,  , , , 
# , , , , , , , , , 
# , ,  , , , , , 墳


@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser("~")
    subprocess.call([home + "/.config/qtile/autostart.sh"])


keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    # Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key(
        [mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"
    ),
    Key(
        [mod, "shift"],
        "l",
        lazy.layout.shuffle_right(),
        desc="Move window to the right",
    ),
    Key(
        [mod],
        "d",
        lazy.spawn(
            "rofi -lines 12 -padding 18 -width 60 -location 0 -show drun -sidebar-mode -columns 3 -font 'JetBrainsMono 14'"
        ),
    ),
    Key([mod], "z", lazy.spawn("emacsclient -c")),
    Key([mod], "x", lazy.spawn("emacsclient -e '(eshell t)' -c")),
    # monitor switching
    # Switch focus of monitors
    Key([mod], "period", lazy.next_screen(), desc="Move focus to next monitor"),
    Key([mod], "comma", lazy.prev_screen(), desc="Move focus to prev monitor"),
    # Switch focus to specific monitor (out of three)
    Key([mod, "shift"], "period", lazy.to_screen(0), desc="Keyboard focus to monitor 1"),
    Key([mod, "shift"], "comma", lazy.to_screen(1), desc="Keyboard focus to monitor 2"),
    # Emacs programs launched using the key chord CTRL+e followed by 'key'
    # KeyChord(
    #     [mod],
    #     "e",
    #     [
    #         Key([], "e", lazy.spawn("emacsclient -c"), desc="Launch Emacs"),
    #         Key(
    #             [],
    #             "b",
    #             lazy.spawn("emacsclient -c --eval '(ibuffer)'"),
    #             desc="Launch ibuffer inside Emacs",
    #         ),
    #         Key(
    #             [],
    #             "d",
    #             lazy.spawn("emacsclient -c --eval '(dired nil)'"),
    #             desc="Launch dired inside Emacs",
    #         ),
    #         Key(
    #             [],
    #             "s",
    #             lazy.spawn("emacsclient -c --eval '(eshell)'"),
    #             desc="Launch the eshell inside Emacs",
    #         ),
    #         Key(
    #             [],
    #             "v",
    #             lazy.spawn("emacsclient -c --eval '(+vterm/here nil)'"),
    #             desc="Launch vterm inside Emacs",
    #         ),
    #     ],
    # ),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key(
        [mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"
    ),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key(
        [mod, "shift"],
        "d",
        lazy.run_extension(
            extension.DmenuRun(
                dmenu_prompt=">",
                dmenu_font="-".join(["JetBrainsMono", "14"]),
                dmenu_bottom=True,
                background=BLACK,
                foreground=GREEN,
                selected_background=GREEN,
                selected_foreground=WHITE,
            )
        ),
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key(
        [mod],
        "space",
        lazy.widget["keyboardlayout"].next_keyboard(),
    ),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -c 0 -q set Master 2dB+")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -c 0 -q set Master 2dB-")),
    Key([], "XF86AudioMute", lazy.spawn("amixer -c 0 -q set Master toggle")),
]

groups = [Group(i) for i in "123456"]
groups[0].label = ""
groups[1].label = ""
groups[2].label = ""
groups[3].label = ""
groups[4].label = ""
groups[5].label = ""
# , , ,  , , , 
# , , , , , , , , , ,  , 

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

layouts = [
    layout.MonadTall(border_width=0, margin=12),
    # layout.Columns(
    #     border_focus_stack=["#d75f5f", "#8f3d3d"],
    #     border_width=0,
    #     margin=10,
    # ),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font="JetBrainsMono",
    fontsize=14,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        wallpaper="/home/pav/Pictures/Wallpapers/ubuntu-3.png",
        wallpaper_mode="fill",
        top=bar.Bar(
            [
                widget.CurrentLayoutIcon(scale=0.4),
                widget.GroupBox(
                    font="JetBrainsMono",
                    fontsize=20,
                    active=colors[6],
                    inactive=colors[1],
                    rounded=False,
                    highlight_color=colors[0],
                    highlight_method="line",
                    this_current_screen_border=colors[0],
                    block_highlight_text_color=colors[2],
                    blockwidth=2,
                    margin_y=3,
                ),
                widget.Sep(
                    padding=10,
                    linewidth=2,
                ),
                widget.WindowName(),
                widget.Spacer(),
                widget.TextBox(
                    text="",
                    foreground=colors[5],
                    padding=0,
                    fontsize=17,
                ),
                widget.CPU(
                    foreground=colors[5],
                    format=" {load_percent}%",
                    font="novamono for powerline bold",
                    fontsize=14,
                ),
                widget.TextBox(
                    text=" ",
                    foreground=colors[5],
                    padding=0,
                    fontsize=17,
                ),
                widget.Memory(
                    foreground=colors[5],
                    font="novamono for powerline bold",
                    fontsize=14,
                    format="{MemUsed: .0f} MB",
                ),
                widget.Battery(
                    foreground=colors[5],
                    fontsize=17,
                    low_percentage=0.2,
                    low_foreground=colors[5],
                    font="fontawesome",
                    update_interval=1,
                    format="{char}",
                    charge_char=" ",
                    discharge_char=" ",
                ),
                widget.Battery(
                    foreground=colors[5],
                    charge_char="↑",
                    discharge_char="↓",
                    font="novamono for powerline bold",
                    fontsize=14,
                    update_interval=1,
                    format="{percent:2.0%} ",
                ),
                widget.TextBox(
                    font="Ubuntu Nerd Font",
                    fontsize=17,
                    foreground=colors[5],
                    padding=0,
                    text="  ",
                ),
                widget.KeyboardLayout(
                    foreground=colors[5],
                    font="JetBrainsMono",
                    configured_keyboards=["us", "el"],
                ),
                widget.TextBox(
                    text=" ",
                    padding=2,
                    fontsize=17,
                    font="JetBrainsMono",
                ),
                widget.Backlight(
                    backlight_name="amdgpu_bl0",
                    change_command="brightnessctl set {0}%",
                    step=2,
                    padding=3,
                ),
                widget.PulseVolume(
                    foreground=colors[5],
                    font="JetBrainsMono",
                    fmt="Vol: {}",
                    padding=5,
                    mouse_callbacks={"Button1": lambda: qtile.cmd_spawn("pavucontrol")},
                ),
                widget.Systray(),
                widget.Sep(
                    padding=5,
                    linewidth=0,
                ),
                widget.Clock(
                    font="JetBrainsMono",
                    fontsize=14,
                    foreground=colors[5],
                    format="%d %b | %A | %I:%M %p",
                ),
                widget.QuickExit(),
            ],
            41,
            background=colors[0],
        ),
    ),
    Screen(
        wallpaper="/home/pav/Pictures/Wallpapers/ubuntu-3.png",
        wallpaper_mode="fill",
        top=bar.Bar(
            [
                widget.CurrentLayoutIcon(scale=0.4),
                widget.GroupBox(
                    font="JetBrainsMono",
                    fontsize=20,
                    active=colors[6],
                    inactive=colors[1],
                    rounded=False,
                    highlight_color=colors[0],
                    highlight_method="line",
                    this_current_screen_border=colors[0],
                    block_highlight_text_color=colors[2],
                    blockwidth=2,
                    margin_y=3,
                ),
                widget.Sep(
                    padding=10,
                    linewidth=2,
                ),
                widget.WindowName(),
                widget.Spacer(),
                widget.TextBox(
                    text="",
                    foreground=colors[5],
                    padding=0,
                    fontsize=17,
                ),
                widget.CPU(
                    foreground=colors[5],
                    format=" {load_percent}%",
                    font="novamono for powerline bold",
                    fontsize=14,
                ),
                widget.TextBox(
                    text=" ",
                    foreground=colors[5],
                    padding=0,
                    fontsize=17,
                ),
                widget.Memory(
                    foreground=colors[5],
                    font="novamono for powerline bold",
                    fontsize=14,
                    format="{MemUsed: .0f} MB",
                ),
                widget.Battery(
                    foreground=colors[5],
                    fontsize=17,
                    low_percentage=0.2,
                    low_foreground=colors[5],
                    font="fontawesome",
                    update_interval=1,
                    format="{char}",
                    charge_char=" ",
                    discharge_char=" ",
                ),
                widget.Battery(
                    foreground=colors[5],
                    charge_char="↑",
                    discharge_char="↓",
                    font="novamono for powerline bold",
                    fontsize=14,
                    update_interval=1,
                    format="{percent:2.0%} ",
                ),
                widget.TextBox(
                    font="Ubuntu Nerd Font",
                    fontsize=17,
                    foreground=colors[5],
                    padding=0,
                    text="  ",
                ),
                widget.KeyboardLayout(
                    foreground=colors[5],
                    font="JetBrainsMono",
                    configured_keyboards=["us", "el"],
                ),
                widget.TextBox(
                    text=" ",
                    padding=2,
                    fontsize=17,
                    font="JetBrainsMono",
                ),
                widget.Backlight(
                    backlight_name="amdgpu_bl0",
                    change_command="brightnessctl set {0}%",
                    step=2,
                    padding=3,
                ),
                widget.PulseVolume(
                    foreground=colors[5],
                    font="JetBrainsMono",
                    fmt="Vol: {}",
                    padding=5,
                ),
                # widget.Systray(),
                widget.Sep(
                    padding=5,
                    linewidth=0,
                ),
                widget.Clock(
                    font="JetBrainsMono",
                    fontsize=14,
                    foreground=colors[5],
                    format="%d %b | %A | %I:%M %p",
                ),
                widget.QuickExit(),
            ],
            41,
            background=colors[0],
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing ⚡
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
#


wmname = "LG3D"
