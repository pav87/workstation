#!/usr/bin/sh
picom -b &
emacs --daemon &
nm-applet &
blueman-applet &
volumeicon &
setxkbmap -model pc104 -layout us,gr -variant ,, -option grp:win_space_toggle &
