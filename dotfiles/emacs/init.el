;; init.el --- Emacs Confuguration  -*- lexical-binding: t; -*-
;;; General editor settings
(setq gc-cons-threshold (* 2 50 1000 1000))
(setq read-process-output-max (* 1024 1024)) ;; 1mb

;; Profile emacs startup
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "*** Emacs loaded in %s seconds with %d garbage collections."
                     (emacs-init-time "%.2f")
                     gcs-done)))
 
(setq inhibit-startup-message t) ;; Disable default startup
(scroll-bar-mode -1)        ;; Disable visible scrollbar
(tool-bar-mode -1)          ;; Disable the toolbar
(tooltip-mode -1)           ;; Disable tooltips
(set-fringe-mode 10)        ;; Provide more breathing room
(menu-bar-mode -1)          ;; Disable menu
;;(display-battery-mode 1)    ;; Display battery life
(setq column-number-mode t) ;; Display colum number
(setq visible-bell t)       ;; Enable error bell
(setq make-backup-files nil) ; stop creating backup~ files
(setq auto-save-default nil) ; stop creating #autosve# files
(defalias 'yes-or-no-p 'y-or-n-p)

(setenv "PATH" (concat (getenv "PATH") ":/home/pav/.nvm/versions/node/v14.13.1/bin/"))
(setq exec-path (append exec-path '("/home/pav/go/bin")))
(setq exec-path (append exec-path '("/home/pav/.nvm/versions/node/v14.13.1/bin/")))

;; iterate through camel case words
(global-subword-mode 1)

(defun find-config ()
  "Edit init file."
  (interactive)
  (find-file "~/pemacs/init.el"))

(global-set-key (kbd "C-c I") 'find-config)

;; Font settings
(set-face-attribute 'default nil :font "Iosevka Comfy" :height 160)

;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)


(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)


(global-set-key (kbd "C-c s") #'rg-menu)

;; Dashboard
(use-package dashboard
  :ensure t
  :config
  (setq dashboard-week-agenda t)
  (setq dashboard-items '((projects . 15) (agenda . 5)))
  (setq dashboard-set-heading-icons t)
  (setq dashboard-page-separator "\n\n\n")
  (setq dashboard-center-content t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-startup-banner 3)
  (setq dashboard-startup-banner "~/pemacs/emacs-dash.png")
  (dashboard-setup-startup-hook))


;; Theme options
(use-package doom-themes)
(setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
      doom-themes-enable-italic t) ; if nil, italics is universally disabled
(load-theme 'doom-snazzy t)
(use-package all-the-icons)


;; Regex
(use-package visual-regexp
  :bind (("C-, C-q" . #'vr/replace)
	 ("C-, C-m" . #'vr/mc-mark)))

;; Region selection
(use-package expand-region
  :bind ("C-=" . er/expand-region))

;; Swiper
(use-package swiper
  :ensure t)

;; Smart parents
(use-package smartparens
  :config (smartparens-global-mode 1))

(use-package elfeed
  :config
  (setq elfeed-feeds
        '("https://planet.emacslife.com/atom.xml"
          "https://sachachua.com/blog/feed/"
	  "https://www.masteringemacs.org/feed"
	  "https://www.inspiredpython.com/feed"
	  "https://pycoders.com/feed"
          "http://karpathy.github.io/feed.xml")))


(use-package transpose-frame
  :bind (("C-, C-h" . flop-frame)))

;; Setup ivy
(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
	 :map ivy-minibuffer-map
	 ("TAB" . ivy-alt-done)
	 ("C-l" . ivy-alt-done)
	 ("C-j" . ivy-next-line)
	 ("C-k" . ivy-previous-line)
	 :map ivy-switch-buffer-map
	 ("C-k" . ivy-previous-line)
	 ("C-l" . ivy-done)
	 ("C-d" . ivy-switch-buffer-kill)
	 :map ivy-reverse-i-search-map
	 ("C-k" . ivy-previous-line)
	 ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 30)))

(use-package flycheck
  :config (add-hook 'after-init-hook #'global-flycheck-mode))

(with-eval-after-load 'flycheck
  (setq-default flycheck-disabled-checkers '(python-mypy python-pylint)))

(with-eval-after-load 'rjsx-mode
  (setq-default flycheck-enabled-checkers '(javascript-tide)))

(use-package sql
  :defer t
  :mode
  (("\\.tsql$" .  sql-mode)))


(use-package lsp-mode
  :ensure t
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "C-c l")
  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
         (sql-mode . lsp-deferred)
         ;; if you want which-key integration
         (lsp-mode . lsp-enable-which-key-integration))
  :commands (lsp lsp-deferred))

;; optionally
(use-package lsp-ui :commands lsp-ui-mode)

;; if you are helm user
(use-package helm-lsp :commands helm-lsp-workspace-symbol)

;; if you are ivy user
(use-package lsp-ivy :commands lsp-ivy-workspace-symbol)
(use-package lsp-treemacs :commands lsp-treemacs-errors-list)

;; optionally if you want to use debugger
(use-package dap-mode)
;; (use-package dap-LANGUAGE) to load the dap adapter for your language

;; (setq lsp-pylsp-plugins-pydocstyle-enabled nil)

;; Company backend
(use-package company
 :ensure t
 :config
 (setq company-idle-delay 0
       company-minimum-prefix-length 2
       company-show-quick-access t
       company-tooltip-limit 10
       company-tooltip-align-annotations t
       ;; invert the navigation direction if the the completion popup-isearch-match
       ;; is displayed on top (happens near the bottom of windows)
       company-tooltip-flip-when-above t)
 (global-company-mode t))

;; Ripgrep
(use-package rg)
(use-package wgrep
  :ensure t
  :custom
  (wgrep-auto-save-buffer t)
  (wgrep-change-readonly-file t))


;; Rust mode

(use-package rust-mode)

;; Python mode
(use-package python
  :config (setq python-indent-guess-indent-offset-verbose nil))

(use-package python-pytest
  :after python
  :bind (("C-c C-m" . #'python-pytest-file)
	 ("C-c t f" . #'python-pytest-file)
	 ("C-c t d" . #'python-pytest-dispatch)))

(use-package pyvenv
  :after python
  :config
  (add-hook 'python-mode-local-vars-hook 'pyvenv-track-virtualenv)
  (pyvenv-mode))

(use-package python-black
  :demand t
  :after python
  :hook (python-mode . python-black-on-save-mode-enable-dwim))

(use-package anaconda-mode
  :ensure t
  :config
  (add-hook 'python-mode-hook 'anaconda-mode)
  (add-hook 'python-mode-hook 'anaconda-eldoc-mode))

(use-package company-anaconda
  :ensure t
  :init (require 'rx)
  :after (company)
  :config
  (add-to-list 'company-backends 'company-anaconda))

(use-package company-quickhelp
  ;; Quickhelp may incorrectly place tooltip towards end of buffer
  ;; See: https://github.com/expez/company-quickhelp/issues/72
  :ensure t
  :config
  (company-quickhelp-mode))

;; optional if you want which-key integration
(use-package which-key
    :config
    (which-key-mode))

;; Setup magit
(use-package magit
  :init
  :config
  :custom
  (git-commit-summary-max-length 50)
  :bind
  (("C-x g" . magit-status)
   ("C-x C-g" . magit-status)))

(use-package diff-hl
  :config
  (global-diff-hl-mode))

;; web setup
(use-package web-mode
  :ensure t
  :hook (web-mode . emmet-mode)
  :mode (("\\.css$" .  web-mode)
	 ("\\.riot$" . web-mode)
         ("\\.html$" .  web-mode))
  :config
  (web-mode-use-tabs))

(use-package emmet-mode
  :after(web-mode css-mode scss-mode)
  :commands (emmet-mode emmet-expand-line yas-insert-snippet company-complete)
  :config
  (setq emmet-move-cursor-between-quotes t)
  (add-hook 'emmet-mode-hook (lambda () (setq emmet-indent-after-insert nil)))
  (add-hook 'sgml-mode-hook 'emmet-mode) ;; Auto-start on any markup modes
  ;;(add-hook 'web-mode-hook 'emmet-mode) ;; Auto-start on any markup modes
  (add-hook 'css-mode-hook  'emmet-mode) ;; enable Emmet's css abbreviation.
  (setq emmet-indentation 2)
  (unbind-key "C-M-<left>" emmet-mode-keymap)
  (unbind-key "C-M-<right>" emmet-mode-keymap)
  :bind
  ("C-j" . emmet-expand-line)
  ((:map emmet-mode-keymap
	 ("C-c [" . emmet-prev-edit-point)
	 ("C-c ]" . emmet-next-edit-point))));end emmet mode

;; Docker
(use-package docker
  :ensure t
  :bind ("C-c d" . docker))

(use-package docker-tramp)

(use-package dockerfile-mode)

(use-package earthfile-mode
  :ensure t)

(use-package yaml-mode
  :mode "\\.yml\\'"
  :config
  (progn (add-hook 'yaml-mode-hook
      (lambda ()
        (define-key yaml-mode-map "\C-m" 'newline-and-indent)))))

(use-package json-mode)

;; vterm
(use-package vterm
  :bind (("C-, C-v" . #'vterm))
  :ensure t)

(use-package ansi-color
  :ensure t
  :config
  (progn
    (defun my/ansi-colorize-buffer ()
      (let ((buffer-read-only nil))
	(ansi-color-apply-on-region (point-min) (point-max))))
    (add-hook 'compilation-filter-hook 'my/ansi-colorize-buffer))
  (add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on))


;; treemacs
(use-package treemacs
  :ensure t
  :defer t
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs                   (if treemacs-python-executable 3 0)
          treemacs-deferred-git-apply-delay        0.5
          treemacs-directory-name-transformer      #'identity
          treemacs-display-in-side-window          t
          treemacs-eldoc-display                   'simple
          treemacs-file-event-delay                5000
          treemacs-file-extension-regex            treemacs-last-period-regex-value
          treemacs-file-follow-delay               0.2
          treemacs-file-name-transformer           #'identity
          treemacs-follow-after-init               t
          treemacs-expand-after-init               t
          treemacs-find-workspace-method           'find-for-file-or-pick-first
          treemacs-git-command-pipe                ""
          treemacs-goto-tag-strategy               'refetch-index
          treemacs-header-scroll-indicators        '(nil . "^^^^^^")'
          treemacs-hide-dot-git-directory          t
          treemacs-indentation                     2
          treemacs-indentation-string              " "
          treemacs-is-never-other-window           nil
          treemacs-max-git-entries                 5000
          treemacs-missing-project-action          'ask
          treemacs-move-forward-on-expand          nil
          treemacs-no-png-images                   nil
          treemacs-no-delete-other-windows         t
          treemacs-project-follow-cleanup          nil
          treemacs-persist-file                    (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-position                        'left
          treemacs-read-string-input               'from-child-frame
          treemacs-recenter-distance               0.1
          treemacs-recenter-after-file-follow      nil
          treemacs-recenter-after-tag-follow       nil
          treemacs-recenter-after-project-jump     'always
          treemacs-recenter-after-project-expand   'on-distance
          treemacs-litter-directories              '("/node_modules" "/.venv" "/.cask")
          treemacs-show-cursor                     nil
          treemacs-show-hidden-files               t
          treemacs-silent-filewatch                nil
          treemacs-silent-refresh                  nil
          treemacs-sorting                         'alphabetic-asc
          treemacs-select-when-already-in-treemacs 'move-back
          treemacs-space-between-root-nodes        t
          treemacs-tag-follow-cleanup              t
          treemacs-tag-follow-delay                1.5
          treemacs-text-scale                      nil
          treemacs-user-mode-line-format           nil
          treemacs-user-header-line-format         nil
          treemacs-wide-toggle-width               70
          treemacs-width                           35
          treemacs-width-increment                 1
          treemacs-width-is-initially-locked       t
          treemacs-workspace-switch-cleanup        nil)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)
    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode 'always)
    (when treemacs-python-executable
      (treemacs-git-commit-diff-mode t))

    (pcase (cons (not (null (executable-find "git")))
                 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple)))

    (treemacs-hide-gitignored-files-mode nil))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t d"   . treemacs-select-directory)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))


(use-package treemacs-all-the-icons
  :after treemacs
  :ensure t
  :config
  (progn
    (treemacs-load-theme "all-the-icons")))

(use-package treemacs-icons-dired
  :hook (dired-mode . treemacs-icons-dired-enable-once)
  :ensure t)

(use-package treemacs-magit
  :after (treemacs magit)
  :ensure t)

(use-package ibuffer-vc
  :after ibuffer
  :ensure t)

(use-package multiple-cursors
  :bind (("C-M-'" . mc/edit-lines)
         ("C-M-|" . mc/mark-all-in-region-regexp)
         ;; Call with a 0 arg to skip one
         ("C-M-." . mc/mark-next-like-this)
         ("C-M-," . mc/mark-previous-like-this)))

(use-package eshell
  :config
  (setq
   eshell-prompt-regexp "^.* λ "
   eshell-aliases-file "~/pemacs/eshell/aliases"))

;; counsel
(use-package counsel
  :bind (("M-x" . counsel-M-x)
         ("C-x b" . counsel-ibuffer)
         ("C-x C-f" . counsel-find-file)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history)))


(use-package popper
  :ensure t ; or :straight t
  :bind (("C-'"   . popper-toggle-latest)
         ("M-`"   . popper-cycle)
         ("C-M-`" . popper-toggle-type))
  :init
  (setq popper-reference-buffers
        '(("^*task:" . hide)
	  "\\*Messages\\*"
          "Output\\*$"
	  "\\*Warnings\\*"
	  "\\*Flycheck errors\\*"
          "\\*Async Shell Command\\*"
	  "^*ssh:"
	  "^*shell:"
	  "^*vterm:"
	  "^\\*eshell.*\\*$" ;eshell as a popup
	  ".shell.$"  ;shell as a popup
	  ".vterm.$"
	  "^*pytest*"
          "^*vterm"  ;vterm as a popup
          help-mode
          compilation-mode))
  (setq popper-window-height 15)
  (popper-mode +1)
  (popper-echo-mode +1))


;; JS
(use-package js2-mode
  :mode "\\.js\\'")

(use-package typescript-mode
  :config ())

(use-package add-node-modules-path)

(use-package rjsx-mode
  :mode "\\.[mc]?js\\'"
  :mode "\\.es6\\'"
  :mode "\\.js\\'"
  :mode "\\.pac\\'"
  :interpreter "node")

;; ensure tab-based indentation is respected in RJSX mode.
(add-hook
 'rjsx-mode-hook
 (lambda () (setq-local indent-line-function 'js-jsx-indent-line)))

(use-package js2-refactor
  :after (js2-mode)
  :hook (js2-mode-hook . js2-refactor-mode))

;; prettier classes with wort vs personal project
;; (use-package prettier-js
;;   :config
;;   (setq prettier-js-args '(
;;                         "--trailing-comma" "es5"
;;                         "--single-quote" "true"
;;                         "--print-width" "100"
;;                         ))
;;   (add-hook 'js2-mode-hook 'prettier-js-mode)
;;   (add-hook 'rjsx-mode-hook 'prettier-js-mode))

(use-package tide
  :ensure t
  :after (rjsx-mode company flycheck)
  :hook ((rjsx-mode . tide-setup)
         (typescript-mode . tide-hl-identifier-mode)))

(eval-after-load 'js-mode
  '(add-hook 'js-mode-hook #'add-node-modules-path))

(eval-after-load 'rjsx-mode
  '(add-hook 'rjsx-mode-hook #'add-node-modules-path))

;; snippets
(use-package yasnippet
    :config
    (add-to-list 'yas-snippet-dirs "~/pemacs/snippets")
    (yas-global-mode 1))


;; org mode
(defun efs/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1))

;; Org Mode Configuration ------------------------------------------------------

(defun efs/org-font-setup ()
  ;; Replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  ;; Set faces for heading levels
  (dolist (face '((org-level-1 . 1.2)
                  (org-level-2 . 1.1)
                  (org-level-3 . 1.05)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.1)
                  (org-level-6 . 1.1)
                  (org-level-7 . 1.1)
                  (org-level-8 . 1.1)))
    (set-face-attribute (car face) nil :font "Iosevka Comfy" :weight 'regular :height (cdr face)))

  ;; unbound org-cycle-agenda-file duplicate key for personal utils
  (define-key org-mode-map (kbd "C-,") nil)

  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch))

(use-package org
  :hook (org-mode . efs/org-mode-setup)
  :config
  (setq org-ellipsis " ▾")
  (efs/org-font-setup))

(use-package org-roam-ui
  :after org-roam
  :config
  (setq org-roam-ui-sync-theme t
        org-roam-ui-follow t
        org-roam-ui-update-on-save t
        org-roam-ui-open-on-start t))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(use-package org-roam
  :after org
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory (file-truename "~/org/roam"))
  (org-roam-completion-everywhere t)
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         :map org-mode-map
         ("C-M-i"    . completion-at-point))
  :config
  (org-roam-setup))

(defun efs/org-mode-visual-fill ()
  (setq visual-fill-column-width 120
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . efs/org-mode-visual-fill))

;; restclient mode
(use-package restclient)

;; custom libraries
(use-package utils
  :ensure nil
  :bind (("C-, C-," . #'utils-chtsh)
	 ("C-, C-x" . #'utils-lexicon)
	 ("C-x p e" . #'utils-project-vterm)
	 ("C-x p t" . #'utils-project-compile-buffer)
	 ("C-, C-l" . #'utils-listify))
  :load-path "~/yorenda/elisp-fu")

(use-package devops
  :ensure nil
  :bind (("C-, C-s" . #'devops-menu)
	 ("C-, C-b" . #'devops-spawn-shell))
  :load-path "~/yorenda/elisp-fu")

(use-package task-runner
  :ensure nil
  :bind (("C-, C-r" . #'task-runner-menu )
	 ("C-, C-t" . #'task-runner-run-task))
  :load-path "~/yorenda/elisp-fu")


(setq electric-pair-mode 1)

;; Keybinds
(global-set-key (kbd "C->") #'end-of-buffer)
(global-set-key (kbd "C-<") #'beginning-of-buffer)
(global-set-key (kbd "C-, C-c") #'shell-command)
(global-set-key (kbd "C-, C-a") #'async-shell-command)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "C-, C-I") 'compile)
(global-set-key (kbd "M-<up>") 'transpose-lines)

;; hooks
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

(setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (shell . t)
   (awk . t)
   (sql . t)))

(setq org-confirm-babel-evaluate nil)
(setq py-python-command "python3")
(setq org-babel-python-command "python3")

(setq custom-file "~/pemacs/custom.el")
(load custom-file)
